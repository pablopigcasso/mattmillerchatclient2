# MattMillerChatClient2

 Created on: February 19, 2019
 Author: Matthew Miller
	  
 I started on the 19th because my birthday was the 18th and I was home in colorado for it, telnet was not working.

 
 This new file was created when I gave up on trying to just update my old main.c file to cover the new project.
 Using the notes from class as well as what I learned from the previous project, here is my attempt at project
 number 2.
 
 
 Acknowledgments:
 - In-class video for help on select: https://www.youtube.com/watch?v=qyFwGyTYe-M
 - Rodkey for new send and recv code: https://westmont.instructure.com/courses/3049/files/folder/Presentations/2019?preview=136453
 - I'm sure there is some stack overflow I could cite, but none of them actually helped. Just gave me a better
 understanding of what I was looking at overall. Not one in particular saved the day though.
 - James Bek helped me a lot with helping me figure out how the heck to use ncurses to make windows
     - Specifically, James showed me the wonders of scollok.
 - Dempsey Salazar showed me how to save my cursor position and recall it.
 - Jared Wilkens helped me figure out threads is easier.
 - http://man7.org/linux/man-pages/man3/pthread_join.3.html the manual page that made this entire project possible
 

To Run the chat client:

- to compile use gcc -o main main.c -lncurses

Open terminal, navigate to run project folder and
type ./main <username>

The program will grab replies from the server automatically
to send text simply type into the lower box and hit enter.
